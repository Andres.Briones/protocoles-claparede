\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{protocole}[2022/09/08 class Latex pour les protocoles de laboratoire]


\LoadClass[11pt]{article}

\RequirePackage[french]{babel} 
\RequirePackage[a4paper,hmargin=2.75cm,vmargin=2.5cm]{geometry} % For the margins
\RequirePackage{fancyhdr} % Header and footer
\RequirePackage{lastpage} % Reference to lastpage
\RequirePackage{xcolor}

\newcommand{\headlinecolor}{\normalcolor}
\definecolor{rouge}{HTML}{D50404}

\DeclareOption{red}{\renewcommand{\headlinecolor}{\color{rouge}}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

% Options for babel
\frenchsetup{StandardLists=false, og=«, fg=»}
\def\frenchfigurename{\sc Figure}
\def\frenchtablename{\sc Tableau}

% Header and footer configuration
\fancyhead[L]{\@title}
\fancyhead[R]{}
\fancyfoot[C]{\thepage /  \pageref{LastPage}}
\fancypagestyle{titlestyle}{
  \fancyhf{}
  \fancyfoot[C]{\thepage /  \pageref{LastPage}}
  \renewcommand{\headrulewidth}{0pt}
}
\pagestyle{fancy}


% Title 
\newcommand{\HRule}{\rule{\linewidth}{0.2mm}} % Defines a new command for the horizontal lines, change thickness here

\renewcommand{\maketitle}{%
  \thispagestyle{titlestyle}
  \begin{center}
    \HRule \\[0.6cm]
    {\Huge \sc \bfseries \headlinecolor \@title}\\[0.4cm] % Title of your  document
    \HRule \\[0.8cm]
  \end{center}
}

% New lengths
\setlength{\parskip}{0.4em} % Spacing between paragraphs 
\setlength{\headheight}{13.6pt}
