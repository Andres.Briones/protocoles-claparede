\documentclass{protocole}

% Packages for input encoding and french accents
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath} % New math environments and commands
\usepackage{gensymb} % For the degree symbol
\usepackage{nicefrac} % Improve inline fractions
\usepackage{graphicx} % More options for the graphics (To use \graphicspath command)
\usepackage{caption} % To use \captionof command 
\usepackage{hyperref} % Hyperreferece options
\usepackage[shortlabels]{enumitem} % Use \begin{enumerate}[A)]... for example

\title{Boussole des Tangentes} % Title 

\graphicspath{{figures/}} % Set directory for figures 

\makeatletter
\let\thetitle\@title % Use the command \thetitle to acces the title name
\makeatother

% Hyper-refereces for the pdf 
\hypersetup{
  pdftitle={\thetitle},
  pdfkeywords={},
  pdfsubject={\thetitle},
  pdfcreator={},
  pdflang={French},
  colorlinks=true,
  linkcolor=black,
  citecolor=black,
  urlcolor=blue}


% ----------------------- Beginning of the document -------------------------


\begin{document}

\maketitle

\section{But de l'expérience}
Mesure de la composante horizontale $B_{Th}$ du champ magnétique terrestre, dont
la valeur officielle, à Genève, est: $B_{Th} = 2.2 \cdot 10^{-5}$ (T) à $5\%$ près.

\section{Matériel à disposition}
\begin{minipage}{.63\linewidth}

  \begin{itemize}
  \item Bobines conductrices plates, verticales, circulaires et concentriques, de
    différents rayons $R$ et de différents nombres de spires $N$, avec leur support
    orientable que l'on peut immobiliser; sur ce support, un alignement des bornes
    électriques facilite le branchement de l'une ou l'autre des bobines au générateur.
  \item Boussole horizontale, fixée sur le support des bobines, en leur centre
    commun, et munie d'un cadran angulaire gradué en degrés, qui permet la mesure
    de la déviation de l'aiguille aimantée.
  \item Source de courant électrique continu et variable.
  \item Ampèrmètre.
  \item Interrupteur.
  \item Fils de connexion.
  \end{itemize}

  \vspace{0.3cm}
\end{minipage}
\begin{minipage}{.37 \linewidth}

  \centering
  \includegraphics[scale = 0.25]{boussole.png}
  \captionof{figure}{Boussole d'inclinaison}
  \label{fig:boussoleInclinaison}

\end{minipage}

\section{Généralités et élements de théorie}



Dans nos régions, en un point donné, le champ magnétique terrestre, grandeur
vectorielle $\vec{B}_T$, a une direction oblique qui forme, avec la verticale du
lieu, un angle dit d'inclinaison de l'ordre de 30\degree. Sa composante
verticale est dirigée vers le bas et sa composante horizontale $\vec{B}_{Th}$
est, à quelques degrés près, dirigée vers le nord (géographique !). Le plan
vertical qui contient le vecteur champ magnétique $\vec{B}_T$ est le méridien
magnétique du lieu.


Pour mesurer la composante horizontale du champ magnétique terrestre
$\vec{B}_{Th}$, on dispose une bobine conductrice plate et circulaire dans le
méridien magnétique du lieu; le diamètre horizontal de la bobine est confondu
avec la direction sud-nord indiquée par l'aiguille aimantée d'axe vertical de la
boussole \textbf{b} (voir figure \ref{fig:directions}).

\begin{figure}[!ht]
  \centering
  \includegraphics[width = 0.95\textwidth]{directions.png}
  \caption{Orientation du dispositif et direction du champ magnétique résultant}
  \label{fig:directions}
\end{figure}

Lorsqu'un courant éléctrique $I$ circule dans la bobine, un champ $\vec{B}_C$,
normal au plan de la bobine (selon l'axe \textbf{y}), s'ajoute à la composante
horizontale du champ terrestre $\vec{B}_{Th}$ et l'aiguille prend la direction
du champ résultant $\vec{B}_{C} + \vec{B}_{Th}$.
L'angle $\alpha$ que forme la direction de l'aiguille aimantée avec la ligne
sud-nord dépend évidement de $|\vec{B}_C|$. On peut écrire : $ \tan(\alpha) =
\nicefrac{|\vec{B}_C|}{|\vec{B}_{Th}|}$.
\newpage \noindent 
Si $N$ et $R$, représentent respectivement le nombre de spires et le rayon de
la bobine, et $I$ le courant électrique qui la parcourt, on peut écrire :
$$
   \tan(\alpha) = \frac{\mu_0\, N\, I \,|\vec{B}_C|}{2\, R\, |\vec{B}_{Th}|}
$$
où $\mu_0$ est la perméabilité magnétique du vide (ou de l'air) : $\mu_0 = 4\pi
\cdot 10^{-7}$ (kg m s$^{-2}$ A$^{-2}$).


\section{Manipulations et mesures}
Confectionnez le circuit électrique, disposez le plan des bobines dans le
méridien magnétique; l'aiguille de la boussole indique la trace horizontale de
ce plan lorsqu'aucun courant électrique ne circule dans la bobine.
Immobilisez le support des bobines, puis procédez à trois jeux de mesures :
\begin{enumerate}[A)]
\item les deux paramètres $I$ et $N$ sont fixés et le rayon $R$ est variable;
  mesurez l'angle $\alpha$, en fonction de $R$.
\item les deux paramètres $I$ et $R$ sont fixés et le nombre de spires $N$ est variable;
  mesurez l'angle $\alpha$, en fonction de $N$.
\item les deux paramètres $N$ et $R$ sont fixés et le courant électrique $I$ est variable;
  mesurez l'angle $\alpha$, en fonction de $I$ (courant maximum de 2,5 (A)).
\end{enumerate}

\section{Présentation des résultats}
 Dans chaque cas A, B, C, faites un tableau des valeurs de la variable ($1/R$,
 $N$, ou $I$), de $\alpha$ et de $\tan(\alpha)$, puis tracez un graphique où la
 variable ($1/R$, $N$, puis $I$) est portée en abcisses, et $\tan(\alpha)$ en
 ordonnées.
 Dans chaque cas, la pente du graphique linéaire conduit à la valeur cherchée de $\vec{B}_{Th}$.

 \begin{figure}[!ht]
   \centering
   \includegraphics[scale = 0.35]{bonhommeDAmpere.png}
   \caption{Le \frquote{bonhomme d'Ampère} déssiné par son créateur}
   \label{fig:bonhommeDAmpere}
 \end{figure}

\end{document}